package Simple;

public class TestString {

	public int testStrBalance (String str, char leftPrm, char rightPrm) {
		int result = 0;
		int leftPrmCount = 0;
		int	rightPrmCount = 0;

		char[] charArray = str.toCharArray();

		for (char c : charArray) {

			if ( c == leftPrm){
				leftPrmCount++;
			} else if ( c == rightPrm) {
				rightPrmCount++;
			}
		}

		if (leftPrmCount == rightPrmCount && leftPrmCount > 0 && rightPrmCount > 0) {
			result = 1;
		} else if (leftPrmCount != rightPrmCount) {
			result = 2;
		} else if ( leftPrmCount == 0 && rightPrmCount == 0) {
			result = 0;
		}

		return result;

	}

}
