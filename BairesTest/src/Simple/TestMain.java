package Simple;

import javax.swing.JOptionPane;

public class TestMain {

	public static void main(String[] args) {
		
		TestString t = new TestString();
		
		String str = "";
		String paramTypeStr = null;
		int paramType = 0;
		int result = 5;
		char leftPrm = 'l';
		char rightPrm = 'p';
		String[] options = {"(   )","[   ]","{   }", "() and []", "() and {}", "[] and {}", "All together"};
		
		paramTypeStr = (String) JOptionPane.showInputDialog(null, "Hello!" 
				+ "	\nThis program will help you analyze if certain symbols are balanced (e.g has the same numbers of OPENS and CLOSES) on a given string. \n\nNow select below which symbol you want to analyze if its balanced: ", "Select the symbol", JOptionPane.QUESTION_MESSAGE, null, options, null);		

		boolean validString = false;
		
		do {
		str = JOptionPane.showInputDialog("Please input the string you want to evaluate below:");
		
		boolean option = true;
		switch (paramTypeStr) {
		case "(   )":
			boolean a = str.contains("(");
			boolean b = str.contains(")");
			
			if (a == false || b == false) {
				option = false;
			}
			break;
		case "[   ]":
			boolean a1 = str.contains("[");
			boolean b1 = str.contains("]");
			
			if (a1 == false || b1 == false) {
				option = false;
			}
			break;
			
		case "{   }":
			boolean a2 = str.contains("{");
			boolean b2 = str.contains("}");
			
			if (a2 == false || b2 == false) {
				option = false;
			}
			break;
			
		case "() and []":
			boolean a3 = str.contains("(");
			boolean b3 = str.contains(")");
			boolean a4 = str.contains("[");
			boolean b4 = str.contains("]");
			
			if (a3 == false || b3 == false || a4 == false || b4 == false) {
				option = false;
			}
			break;
			
		case "() and {}":
			boolean a5 = str.contains("(");
			boolean b5 = str.contains(")");
			boolean a6 = str.contains("{");
			boolean b6 = str.contains("}");
			
			if (a5 == false || b5 == false || a6 == false || b6 == false) {
				option = false;
			}
			break;
			
		case "[] and {}":
			boolean a7 = str.contains("[");
			boolean b7 = str.contains("]");
			boolean a8 = str.contains("{");
			boolean b8 = str.contains("}");
			
			if (a7 == false || b7 == false || a8 == false || b8 == false) {
				option = false;
			}
			break;
			
		case "All together":
			boolean d1 = str.contains("[");
			boolean e1 = str.contains("]");
			boolean f1 = str.contains("{");
			boolean g1 = str.contains("}");
			boolean h1 = str.contains("(");
			boolean j1 = str.contains(")");
			
			if (d1 == false || e1 == false || f1 == false || g1 == false || h1 == false || j1 == false) {
				option = false;
			}
			break;
		default:
			break;
		}		
		
		if (option == false) {
			JOptionPane.showMessageDialog(null, "This string does not contains the symbols to be evaluated, so, it can't be balanced anyway");
			validString = false;
		} else {
			validString = true;
		}
		}while (validString == false);
		
		switch (paramTypeStr) {
		case "(   )":
			leftPrm = '(';
			rightPrm = ')';
			break;
		case "[   ]":
			leftPrm = '[';
			rightPrm = ']';
			break;
		case "{   }":
			leftPrm = '{';
			rightPrm = '}';
			break;
		default:
			break;
		}		
				
		if (paramTypeStr.equalsIgnoreCase("() and []")) {
			int resultprn = t.testStrBalance(str, '(', ')');
			int resultsqr = t.testStrBalance(str, '[', ']');

			if (resultprn == 1 && resultsqr == 1) {
				result = 1;

			} else {
				result = 2;
			}

		} else if (paramTypeStr.equalsIgnoreCase("() and {}")){
			int resultprn = t.testStrBalance(str, '(', ')');
			int resultbrk = t.testStrBalance(str, '{', '}');

			if (resultprn == 1 && resultbrk == 1) {
				result = 1;

			} else {
				result = 2;
			}

		} else if (paramTypeStr.equalsIgnoreCase("[] and {}")){
			int resultsqr = t.testStrBalance(str, '[', ']');
			int resultbrk = t.testStrBalance(str, '{', '}');

			if (resultsqr == 1 && resultbrk == 1) {
				result = 1;

			} else {
				result = 2;
			}

		}
		else if (paramTypeStr.equalsIgnoreCase("All together")){
			int resultprn = t.testStrBalance(str, '(', ')');
			int resultsqr = t.testStrBalance(str, '[', ']');
			int resultbrk = t.testStrBalance(str, '{', '}');

			if (resultprn == 1 && resultsqr == 1 && resultbrk == 1) {
				result = 1;

			} else {
				result = 2;
			}

		} else {
			result= t.testStrBalance(str, leftPrm, rightPrm);
		}
		
		
		String msg = "";
		
		switch (result) {
		case 1: 
			msg = "Your input string was:\n    " + str
			+     "\n\nThe params you choose were: " + paramTypeStr
			+     "\n\nThe parameters are balanced in that string."; 
			break;
			
		case 2: 
			msg = "Your input string was:\n    " + str
			+     "\n\nThe params you choose were: " + paramTypeStr
			+     "\n\nThe parameters are NOT balanced in that string."; 
			break;
			
		case 0: 
			msg = "Your input string was:\n    " + str
			+     "\n\nThe params you choose were: " + paramTypeStr
			+     "\n\nThe parameters doesn't even exist in that String."; 
			break;
		default:
			break;
		}
		
		JOptionPane.showMessageDialog(null, msg);
	}

}
